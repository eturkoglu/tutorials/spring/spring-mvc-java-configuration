<!DOCTYPE html>

<html>

<head>
  <meta charset="UTF-8">
  <title>Spring MVC</title>
</head>

<body>
  
  <h3>Customer Confirmation</h3>
  
  <hr>
  
  Customer is confirmed: ${customer.firstName} ${customer.lastName} 
  
  <br/><br/>
  
  Free Passes: ${customer.freePasses}
  
  <br/><br/>
  
  Postal Code: ${customer.postalCode}
  
  <br/><br/>
  
  Course Code: ${customer.courseCode}
  
  <br/><br/>
    
</body>

</html>