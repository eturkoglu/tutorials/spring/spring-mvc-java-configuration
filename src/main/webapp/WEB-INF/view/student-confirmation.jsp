<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<html>

<head>
  <meta charset="UTF-8">
  <title>Spring MVC</title>
</head>

<body>
  
  <h3>Student Confirmation</h3>
  
  <hr>
  
  Student is confirmed: ${student.firstName} ${student.lastName} 
  
  <br/><br/>
  
  Country: ${student.country}
  
  <br/><br/>
  
  State: ${student.state}
  
  <br/><br/>
  
  Team: ${student.team}
  
  <br/><br/>
  
  Favorite Language: ${student.favoriteLanguage}
  
  <br><br>
	
  Operating Systems : 

  <ul>
	<c:forEach var="temp" items="${student.operatingSystems}">
	  <li>${temp}</li>
	</c:forEach>
  </ul>
  
</body>

</html>