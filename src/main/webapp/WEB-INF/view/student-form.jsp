<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<html>

<head>
  <meta charset="UTF-8">
  <title>Spring MVC</title>
</head>

<body>
  
  <form:form method="post" modelAttribute="student">
    
    First Name: <form:input path="firstName" />
    
    <br/><br/>
    
    Last Name: <form:input path="lastName" />
    
    <br/><br/>
    
    Country: 
    <form:select path="country">
      <form:option value="Brazil" label="Brazil" />
      <form:option value="Germany" label="Germany" />
      <form:option value="England" label="England" />
      <form:option value="Turkey" label="Turkey" />
    </form:select>
    
    <br/><br/>
    
    <form:select path="state">
      <form:options items="${student.states}"/>
    </form:select>
    
    <br/><br/>
    
    <form:select path="team">
      <form:options items="${teams}"/>
    </form:select>
    
    <br/><br/>
    
    Favorite Language:
    Java <form:radiobutton path="favoriteLanguage" value="Java"/>
    Ruby <form:radiobutton path="favoriteLanguage" value="Ruby"/>
    Kotlin <form:radiobutton path="favoriteLanguage" value="Kotlin"/>
    Python <form:radiobutton path="favoriteLanguage" value="Python"/>
    
    <br/><br/>
    
    <br><br>
		
	Operating Systems:
		
	Linux <form:checkbox path="operatingSystems" value="Linux" />
	Mac OS <form:checkbox path="operatingSystems" value="Mac OS" />
	MS Windows <form:checkbox path="operatingSystems" value="MS Windows" />
    
    <br/><br/>
    
    <input type="submit" value="Submit" />
  </form:form>
  
</body>

</html>