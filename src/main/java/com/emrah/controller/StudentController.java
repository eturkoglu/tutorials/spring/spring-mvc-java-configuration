package com.emrah.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

import com.emrah.model.Student;

@Controller
@RequestMapping("/student")
public class StudentController {
	
	@Value("#{teams}") 
	private Map<String, String> teams;

	@GetMapping
	public String show(Model model) {
		model.addAttribute("student", new Student());
		model.addAttribute("teams", teams);
		return "student-form";
	}
	
	@PostMapping
	public String process(@ModelAttribute Student student) {
		return "student-confirmation";
	}
	
}
