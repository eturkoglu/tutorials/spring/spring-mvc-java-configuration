package com.emrah.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/hello")
public class HelloController {

	@GetMapping
	public String show() {
		return "hello-form";
	}
	
/*
	@PostMapping
	public String process() {
		return "hello-response";
	}
*/

/*
	@PostMapping
	public String process(HttpServletRequest request, Model model) {
		String name = request.getParameter("name");
		String message = "Yo! " + name.toUpperCase();
		model.addAttribute("message", message);
		return "hello-response";
	}
*/
	
	@PostMapping
	public String process(@RequestParam String name, Model model) {
		String message = "Yo! " + name.toUpperCase();
		model.addAttribute("message", message);
		return "hello-response";
	}
	
}
