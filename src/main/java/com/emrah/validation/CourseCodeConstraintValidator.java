package com.emrah.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CourseCodeConstraintValidator implements ConstraintValidator<CourseCode, String>{

	/*private String coursePrefix;*/
	private String[] coursePrefixes;
	
	@Override
	public void initialize(CourseCode courseCode) {
		coursePrefixes = courseCode.value();
	}
	
	@Override
	public boolean isValid(String code, ConstraintValidatorContext constraintValidatorContext) {
	
		var result = false;
		
		if(code != null) {
			/*result = code.startsWith(coursePrefix);*/
			
			for (var tempPrefix : coursePrefixes) {
                result = code.startsWith(tempPrefix);
                
                // if we found a match then break out of the loop
                if (result) {
                    break;
                }
            }
			
		} else {
			result = false;
		}
		
		return result;
	}

}
